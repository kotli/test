package org.kotlin.test;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.kotlin.client.Robot;

public class RobotTests {

    @Test
    @DisplayName("test Robot")
    public void testRobot(){
        Robot robot = new Robot("localhost", 50007);
        robot.sendToRobot("hello");
    }
}
