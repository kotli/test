import socket, time

serversocket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
serversocket.bind(('0.0.0.0', 50007))
serversocket.listen(5) # become a server socket, maximum 5 connections
connection, address = serversocket.accept()
print("connected from ", address)

connection.send("msg from python".encode('utf-8'))
buf = connection.recv(1024)
print (buf.decode('utf-8'))